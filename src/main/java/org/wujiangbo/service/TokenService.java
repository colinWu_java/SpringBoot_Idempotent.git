package org.wujiangbo.service;

import javax.servlet.http.HttpServletRequest;

public interface TokenService {

    //创建token
    String createToken();

    //检验token
    boolean checkToken(HttpServletRequest request) throws Exception;
}
