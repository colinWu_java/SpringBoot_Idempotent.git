package org.wujiangbo.controller;

import lombok.extern.slf4j.Slf4j;
import org.wujiangbo.annotation.CheckIdempotent;
import org.wujiangbo.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class TestController {

    @Autowired
    private TokenService tokenService;

    //获取token值
    @GetMapping("/getToken")
    public String getToken(){
        return tokenService.createToken();
    }

    /**
     * 保存用户信息
     */
    @PostMapping("/saveUser")
    @CheckIdempotent
    public String saveUser(){
        log.info("保存用户");
        return "add user success";
    }
}